import pytest
from shoppingcart.cart import ShoppingCart
from shoppingcart.cart import Products
from shoppingcart.cart import CurrencyConverter


@pytest.fixture(scope="module")
def products():
    yield Products()


@pytest.fixture(scope="module")
def converter():
    yield CurrencyConverter()


def test_add_item(products, converter):
    cart = ShoppingCart(products=products, converter=converter)
    cart.add_item("apple", 1)
    receipt = cart.print_receipt()
    assert receipt[0] == "Granny Smith (Apples) - 1 - EUR 1.00"


def test_remove_item(products, converter):
    cart = ShoppingCart(products=products, converter=converter)
    cart.add_item("apple", 2)
    cart.remove_item("apple", 1)
    receipt = cart.print_receipt()
    assert receipt[0] == "Granny Smith (Apples) - 1 - EUR 1.00"


def test_add_item_with_multiple_quantity(products, converter):
    cart = ShoppingCart(products=products, converter=converter)
    cart.add_item("apple", 2)

    receipt = cart.print_receipt()

    assert receipt[0] == "Granny Smith (Apples) - 2 - EUR 2.00"


def test_add_different_items(products, converter):
    cart = ShoppingCart(products=products, converter=converter)
    cart.add_item("banana", 1)
    cart.add_item("kiwi", 1)

    receipt = cart.print_receipt()

    assert receipt[0] == "Yellow Bananas - 1 - EUR 1.10"
    assert receipt[1] == "Green Kiwi - 1 - EUR 3.00"


def test_different_currencies(products, converter):
    cart = ShoppingCart(products=products, converter=converter)
    c = cart.converter.eur_to("GBP")

    cart.add_item("apple", 2)
    price = cart.products.get_price("apple") * c

    receipt = cart.print_receipt(currency="GBP")

    assert receipt[0] == f"Granny Smith (Apples) - 2 - GBP {(price * 2):.2f}"
