import typing
from pathlib import Path
from collections import OrderedDict
import json

from . import abc


class CurrencyError(LookupError):
    def __init__(self, arg):
        self.strerror = arg
        self.args = {arg}


class CurrencyConverter():
    def __init__(self, **kwargs):
        import requests
        from requests.exceptions import HTTPError
        # TODO: probably not the best place for import,
        # however, given that this class itself is a mockup,
        # I'll leave the import here.
        try:
            response = requests.get('https://api.exchangeratesapi.io/latest')
        except HTTPError:
            self.data = json.load("rates.json")
            print("Using cached exchange rates")
        else:
            if response.status_code == 200:
                self.rates = json.loads(response.text)['rates']
            else:
                self.data = json.load("rates.json")
                print("Using cached exchange rates")

    def eur_to(self, currency):
        if currency in self.rates:
            return self.rates[currency]
        else:
            raise CurrencyError(f"Rate for {currency} not available.")


class Products():
    def __init__(self, **kwargs):

        if 'filename' in kwargs:
            filename = Path(kwargs['filename'])
        else:
            filename = Path("prices.json")

        if not filename.exists():
            raise FileNotFoundError

        with open(filename) as json_data:
            self.data = json.load(json_data)

    def get_price(self, product_code: str):
        return self.data[product_code]['price']

    def get_title(self, product_code: str):
        return self.data[product_code]['title']


class ShoppingCart(abc.ShoppingCart):
    def __init__(self, **kwargs):
        self._items = OrderedDict()
        self.products = kwargs['products']
        self.converter = kwargs['converter']

    def add_item(self, product_code: str, quantity: int):
        if product_code not in self._items:
            self._items[product_code] = quantity
        else:
            q = self._items[product_code]
            self._items[product_code] = q + quantity

    def remove_item(self, product_code: str, quantity: int):
        if product_code in self._items:
            self._items[product_code] -= quantity

        if self._items[product_code] <= 0:
            del self._items[product_code]

    def print_receipt(self, **kwargs) -> typing.List[str]:
        lines = []
        total = 0.0

        if 'currency' in kwargs:
            currency = kwargs['currency']
        else:
            currency = "EUR"

        for item, quantity in self._items.items():
            price, currency = self._get_product_price(item, currency=currency)
            total += price * quantity
            lines.append(
                f"{self.products.get_title(item)} - "
                f"{quantity} - {currency} {(price * quantity):.2f}"
                )
        lines.append(f"TOTAL - {currency} {total:.2f}")

        return lines

    def _get_product_price(self, product_code: str, **kwargs) -> tuple:
        price = self.products.get_price(product_code)
        if 'currency' in kwargs:
            currency = kwargs['currency']
            if currency != "EUR":
                converted = self.converter.eur_to(currency) * price
                return (converted, currency)
            else:
                return (price, "EUR")
        else:
            return (price, "EUR")
