# Shopping till system - 2019-12-02

In this assignment I am tasked to refactor a shopping till system and make couple of improvements.

First thing I noticed was that this project doesn't include `requirements.txt` nor `Pipfile`. The tests seemed standard and compatible with _pytest_, so I started this refactorization project with setting up environment and installing _pytest_ package.

```bash
pipenv install pytest
```

Next I tried the tests before changing anything.
```bash
cd test-shoppingcart
pipenv run pytest
```

Here are the tasks requested:

#### Make the receipt print items in the order that they were added
I achieved this by simply swapping `dict()` with `OrderedDict()` from `collections` module.

#### Add a 'Total' line to the receipt. This should be the full price we should charge the customer
Added.

#### Be able to fetch product prices from an external source (json file, database ...)
I implemented a class `Product` with functions `get_price()` and `get_title()`.
Current implementation gets products from a json file.

#### Be able to display the product prices in different currencies (not only Euro).
To be able to print receipts in different currencies, I implemented class `CurrencyConverter` which tries to fetch the latest exchanges rates from Internet. I choose to implement it as a class because it can be instantiated once and used many times later. A better solution for production environment would be a key-value storage with scheduled refresh.

#### Update the test suite to extend coverage and limit the number of tests which need changing when changes are introduced
Test suite updated.
